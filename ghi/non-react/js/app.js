function createCard(name, description, pictureUrl, dateRange, location) {
    return `
      <div class="card" style="margin-bottom:1em;box-shadow:0.5em 0.5em 1em black;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${dateRange}</div>
      </div>`;
}
function createCardPlaceholder() {
    return `
      <div class="card-placeholder" aria-hidden="true">
      <svg class="bd-placeholder-img card-img-top" width="100%" height="600" role="img" aria-label="Placeholder">
        <rect width="100%" height="100%" fill="lightblue" opacity="0.3"></rect>
      </svg>
        <div class="card-body">
          <h5 class="card-title placeholder-glow">
            <span class="placeholder col-6"></span>
          </h5>
          <h6 class="card-subtitle mb-2 text-muted">
            <span class="placeholder col-6"></span>
          </h6>
          <p class="card-text placeholder-glow">
            <span class="placeholder col-7"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-6"></span>
            <span class="placeholder col-8"></span>
          </p>
        </div>
        <div class="card-footer">
          <span class="placeholder col-6"></span>
        </div>
      </div>
    `;
  }
  
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            const wrapper = document.querySelector(".container");
            wrapper.innerHTML = `
            <div class="alert alert-warning" role="alert">
              ${response.status}: server not running
            </div>`;
        } else {
            const data = await response.json();
            const columns = document.querySelectorAll('.col');
            let cycle = 0;
            for (let i = 0; i < data.conferences.length; i++) {
                const html = createCardPlaceholder();
                const column = columns[cycle];
                column.innerHTML += html;
                column.innerHTML;
                if (cycle === columns.length - 1) {
                    cycle = 0;
                } else {
                    cycle++;
                }
            }
            cycle = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts);
                    const endDate = new Date(details.conference.ends);
                    const dateRange = `${startDate.getMonth()}/${startDate.getDay()}/${startDate.getFullYear()} - ${endDate.getMonth()}/${endDate.getDay()}/${endDate.getFullYear()}`;
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, dateRange, location);
                    const column = columns[cycle];
                    const placeholder = column.querySelector(".card-placeholder").outerHTML;
                    column.innerHTML = column.innerHTML.replace(placeholder, html);
                    if (cycle === columns.length - 1) {
                        cycle = 0;
                    } else {
                        cycle++;
                    }
                }
            }
        }
    } catch (e) {
        const wrapper = document.querySelector(".container");
        wrapper.innerHTML = `
        <div class="alert alert-danger" role="alert">
          ${e}
        </div>`;
    }
});
