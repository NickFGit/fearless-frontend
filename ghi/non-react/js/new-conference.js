window.addEventListener('DOMContentLoaded', async () => {
    const locationsURL = "http://localhost:8000/api/locations/";
    try {
        const response = await fetch(locationsURL);
        if (!response.ok) {
            const wrapper = document.getElementsByName("body");
            wrapper.innerHTML = `
            <div class="alert alert-warning" role="alert">
              ${response.status}: server not running
            </div>`;
        } else {
            const locations = await response.json();
            const locationDropdown = document.getElementById("location");
            for (let location of locations.locations) {
                locationDropdown.innerHTML += `<option value=${location.href[location.href.length - 2]}>${location.name}</option>`;
            }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const conferenceURL = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceURL, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                }
            });
        }
    } catch (e) {
        const wrapper = document.getElementsByName("body");
        wrapper.innerHTML = `
        <div class="alert alert-danger" role="alert">
          ${e}
        </div>`;
    }
});