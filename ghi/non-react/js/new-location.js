
window.addEventListener('DOMContentLoaded', async () => {
    const statesURL = "http://localhost:8000/api/states/";
    try {
        const response = await fetch(statesURL);
        if (!response.ok) {
            const wrapper = document.querySelector(".container");
            wrapper.innerHTML = `
            <div class="alert alert-warning" role="alert">
              ${response.status}: server not running
            </div>`;
        } else {
            const states = await response.json();
            const statesDropdown = document.getElementById("state");
            for (let state of states.states) {
                statesDropdown.innerHTML += `<option value="${state.abbreviation}">${state.name}</option>`;
            }
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                }
            });
        }
    } catch (e) {
        const wrapper = document.getElementsByName("body");
        wrapper.innerHTML = `
        <div class="alert alert-danger" role="alert">
          ${e}
        </div>`;
    }
});