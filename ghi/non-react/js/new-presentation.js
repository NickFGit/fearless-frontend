window.addEventListener('DOMContentLoaded', async () => {
    const conferencesURL = "http://localhost:8000/api/conferences/";
    try {
        const response = await fetch(conferencesURL);
        if (!response.ok) {
            const wrapper = document.getElementsByName("body");
            wrapper.innerHTML = `
            <div class="alert alert-warning" role="alert">
              ${response.status}: server not running
            </div>`;
        } else {
            const conferences = await response.json();
            const conferenceDropdown = document.getElementById("conference");
            for (let conference of conferences.conferences) {
                conferenceDropdown.innerHTML += `<option value=${conference.href[conference.href.length - 2]}>${conference.name}</option>`;
            }
            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const select = document.getElementById("conference");
                const presentationURL = `http://localhost:8000/api/conferences/${select.options[select.selectedIndex].value}/presentations/`;
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(presentationURL, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                }
            });
        }
    } catch (e) {
        const wrapper = document.getElementsByName("body");
        wrapper.innerHTML = `
        <div class="alert alert-danger" role="alert">
          ${e}
        </div>`;
    }
});