window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (response.ok) {
            const selectTag = document.getElementById('conference');
            const data = await response.json();
        
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
            const loadingIcon = document.getElementById("loading-conference-spinner");
            loadingIcon.classList.add("d-none");
            selectTag.classList.remove("d-none");
            const formTag = document.getElementById('create-attendee-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch("http://localhost:8001/api/attendees/", fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const messageTag = document.getElementById("success-message");
                    formTag.classList.add("d-none");
                    messageTag.classList.remove("d-none");
                }
            });
        } else {
            const wrapper = document.getElementsByName("body");
            wrapper.innerHTML = `
            <div class="alert alert-warning" role="alert">
            ${response.status}: server not running
            </div>`;
        }
    } catch (e) {
        const wrapper = document.getElementsByName("body");
        wrapper.innerHTML = `
        <div class="alert alert-danger" role="alert">
        ${e}
        </div>`;
    }
  
  });