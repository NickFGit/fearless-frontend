import React , { useEffect, useState } from 'react';

function PresentationForm() {

  const[conferences, setConferences] = useState([]);
  const [conference, setConference] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
 
  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  }

  const handleCompanyNameChange = (event) => {
    setCompanyName(event.target.value);
  }

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  }

  const handleSynopsisChange = (event) => {
    setSynopsis(event.target.value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.email = email;
    data.conference = conference;
    data.title = title;
    data.company_name = companyName;
    data.synopsis = synopsis;
    console.log(data);
    const attendeesUrl = "http://localhost:8001/api/presentations/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(attendeesUrl, fetchConfig);
    if (response.ok) {
      const newAttendee = await response.json();
      setConference("");
      setName("");
      setEmail("");
      setCompanyName("");
      setTitle("");
      setSynopsis("");
    }
  }

  const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
  }

  useEffect(() => {fetchData();}, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presenation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" name="presenter_name" required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} value={email} placeholder="example@email.com" name="presenter_email" required type="email" id="email" className="form-control" />
                <label htmlFor="email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} value={companyName} placeholder="Example, Inc." name="company_name" type="text" id="company" className="form-control" />
                <label htmlFor="company">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Exciting Presentation" name="title" required type="text" id="title" className="form-control" />
                <label htmlFor="title">Presentation title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" placeholder="Short summary here..." name="synopsis" required type="text" id="synopsis"></textarea>
              </div>
              <div className="mb-3">
              <select onChange={handleConferenceChange} value={conference} name="conference" id="conference" className="form-select" required>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                  return <option key={conference.href} value={conference.href}>{conference.name}</option>
                  })}
              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}


export default PresentationForm;