import React , { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState("");
    const [name, setName] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");
    const [description, setDescription] = useState("");
    const [maxPresentations, setMaxPresentations] = useState(0);
    const [maxAttendees, setMaxAttendees] = useState(0);

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.description = description;
        data.starts = start;
        data.ends = end;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = Number(location);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            setLocation("");
            setName("");
            setStart("");
            setEnd("");
            setDescription("");
            setMaxPresentations(0);
            setMaxAttendees(0);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
            console.log(locations);
        }
    }

    useEffect(() => {fetchData();}, []);

    return (   
    <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea onChange={handleDescriptionChange} value={description} className="form-control" placeholder="The premier event to learn..." name="description" required type="text" id="description"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartChange} value={start} placeholder="mm/dd/yyyy" name="starts" required type="date" id="start" className="form-control" />
                            <label htmlFor="start">Start Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndChange} value={end} placeholder="mm/dd/yyyy" name="ends" required type="date" id="end" className="form-control" />
                            <label htmlFor="end">End Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="" name="max_presentations" required type="number" id="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="" name="max_attendees" required type="number" id="max_attendees" className="form-control" />
                            <label htmlFor="max_attendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return <option key={location.href} value={location.href[location.href.length - 2]}>{location.name}</option>
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    );
}

export default ConferenceForm;